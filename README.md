# Pullserver Build and Configuration

1. Ensure that you have PowerShell 5.1 or greater.
1. Create a local configuration temp folder to server as a scratch space for compiling the meta MOF.
1. Open a PowerShell prompt as Admin and execute Install-Module –Name xPSDesiredStateConfiguration.
1. Get an SSL certificate for the DSC Pull server from a trusted Certificate Authority and install the certificate in CERT:\LocalMachine\My. Make a note of the certificate thumbprint.
1. In the PowerShell prompt type New-Guid(), note the resulting GUID.
1. Open Windows PowerShell ISE as admin and paste in the following script, substituting the thumbprint, registration key and local configuration temp folder with those noted above.

    ```powershell
    configuration xDscPullServer{
        param(
            [string[]]$NodeName = 'localhost',
            [Parameter(Mandatory)]
            [ValidateNotNullOrEmpty()]
            [string] $certificateThumbPrint,
            [Parameter(Mandatory)]
            [ValidateNotNullOrEmpty()]
            [string] $RegistrationKey
        )
        Import-DSCResource -ModuleName xPSDesiredStateConfiguration
        Import-DSCResource –ModuleName PSDesiredStateConfiguration
        Node $NodeName{
            WindowsFeature DSCServiceFeature{
                Ensure = 'Present'
                Name = 'DSC-Service'
            }

        WindowsFeature Web-MgmtTools{
            Ensure = 'Present'
            Name = 'Web-Mgmt-Tools'
            DependsOn = '[WindowsFeature]DSCServiceFeature'
        }

        xDscWebService pullserverdev{
            Ensure = 'Present'
            EndpointName = 'pullserverdev'
            Port = 8080
            PhysicalPath = "$env:SystemDrive\inetpub\pullserverdev
            CertificateThumbPrint = $certificateThumbPrint
            ModulePath = "$env:PROGRAMFILES\WindowsPowerShell\DscService\Modules"
            ConfigurationPath = "$env:PROGRAMFILES\WindowsPowerShell\DscService\Configuration"
            State = 'Started'
            DependsOn = '[WindowsFeature]DSCServiceFeature'
            UseSecurityBestPractices = $false
            RegistrationKeyPath = "$env:ProgramFiles\WindowsPowerShell\DscService"
            AcceptSelfSignedCertificates = $true
        }

        File RegistrationKeyFile{
            Ensure = 'Present'
            Type = 'File'
            DestinationPath = "$env:ProgramFiles\WindowsPowerShell\DscService\RegistrationKeys.txt"
            Contents = $RegistrationKey
        }
    }

    xDSCPullServer -certificateThumbprint '[CERTIFICATE THUMBPRINT]' -RegistrationKey '[REGISTRATION KEY]' -OutputPath [LOCAL CONFIGURATION TEMP FOLDER]

    Start-DscConfiguration -Path [LOCAL CONFIGURATION TEMP FOLDER] -Wait -Verbose
    ```

1. Reboot the server when the process is complete.
1. Confirm the installation by navigating to https://[SERVER HOSTNAME]:8080/PSDSCPullServer.svc.
1. To create a certificate for MOF encryption, paste the following in a new tab in the Windows PowerShell ISE substituting [PASSWORD] for a newly defined certificate password.

    ```powershell
    . .\New-SelfSignedCertificateEx.ps1
    New-SelfSignedCertificateEx `
        -Subject "CN=${ENV:ComputerName}" `
        -EKU 'Document Encryption' `
        -KeyUsage 'KeyEncipherment, DataEncipherment' `
        -SAN ${ENV:ComputerName} `
        -FriendlyName 'DSC Credential Encryption certificate' `
        -Exportable `
        -StoreLocation 'LocalMachine' `
        -KeyLength 2048 `
        -ProviderName 'Microsoft Enhanced Cryptographic Provider v1.0' `
        -AlgorithmName 'RSA' `
        -SignatureAlgorithm 'SHA256'

    # Locate the newly created certificate
    $Cert = Get-ChildItem -Path cert:\LocalMachine\My `
        | Where-Object {
            ($_.FriendlyName -eq 'DSC Credential Encryption certificate') `
            -and ($_.Subject -eq "CN=${ENV:ComputerName}")
        } | Select-Object -First 1

    # export the public key certificate
    $mypwd = ConvertTo-SecureString -String "[PASSWORD]" -Force -AsPlainText
    $cert | Export-PfxCertificate -FilePath ".\DscPrivateKey.pfx" -Password $mypwd -Force

    # remove the private key certificate from the node but keep the public key certificate
    $cert | Export-Certificate -FilePath ".\DscPublicKey.cer" -Force

    $cert | Remove-Item -Force
    Import-Certificate -FilePath ".\DscPublicKey.cer" -CertStoreLocation Cert:\LocalMachine\My
    ```
