﻿configuration DscPullServer{
    param(
        [string[]]$NodeName = 'localhost',

        [Parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [string] $certificateThumbPrint,

        [Parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [string] $RegistrationKey
    )

    Import-DSCResource -ModuleName xPSDesiredStateConfiguration
    Import-DSCResource -ModuleName PSDesiredStateConfiguration

    Node $NodeName{
        WindowsFeature DSCServiceFeature{
            Ensure = 'Present'
            Name = 'DSC-Service'
        }

        WindowsFeature Web-MgmtTools{
            Ensure = 'Present'
            Name = 'Web-Mgmt-Tools'
            DependsOn = '[WindowsFeature]DSCServiceFeature'
        }

        xDscWebService pullserver{
            Ensure = 'Present'
            EndpointName = 'pullserver'
            Port = 8080
            PhysicalPath = "c:\inetpub\wwwroot\pullserver"
            CertificateThumbPrint = $certificateThumbPrint
            ModulePath = "$($env:PROGRAMFILES)\WindowsPowerShell\DscService\Modules"
            ConfigurationPath = "$($env:PROGRAMFILES)\WindowsPowerShell\DscService\Configuration"
            State = 'Started'
            DependsOn = '[WindowsFeature]DSCServiceFeature'
            UseSecurityBestPractices = $false
            RegistrationKeyPath = "$($env:ProgramFiles)\WindowsPowerShell\DscService"
            AcceptSelfSignedCertificates = $true
        }

        File RegistrationKeyFile{
            Ensure = 'Present'
            Type = 'File'
            DestinationPath = "$env:ProgramFiles\WindowsPowerShell\DscService\RegistrationKeys.txt"
            Contents = $RegistrationKey
        }
    }
}

If(Test-Path -Path "$($env:TEMP)\DSCPullServer" -eq $false){
    New-Item -ItemType Directory -Path "$($env:TEMP)\DSCPullServer"
}
DSCPullServer -certificateThumbprint '48076A55C75F4EF748F33B1C2D4FFB2F7D5C53B2' -RegistrationKey '1115d003-28b4-4100-8499-8854101b321a' -OutputPath "$($env:TEMP)\DSCPullServer"
Start-DscConfiguration -Path "$($env:TEMP)\DSCPullServer" -Wait -Verbose